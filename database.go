package main

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

func initDB(url string) *sqlx.DB {
	db, err := sqlx.Connect("mysql", url)
	if err != nil {
		panic(err)
	}

	return db
}

func createDBAndTables(user, password, dbName string) *sqlx.DB {
	url := fmt.Sprintf("%s:%s@tcp(localhost:3306)/", user, password)
	db := initDB(url)
	db.MustExec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName))

	url = fmt.Sprintf("%s:%s@tcp(localhost:3306)/%s", user, password, dbName)
	db = initDB(url)
	db.MustExec(`
		CREATE TABLE IF NOT EXISTS todos (id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, task TEXT);
	`)

	return db
}
