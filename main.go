package main

import (
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	user := os.Getenv("RINGOVER_USER")
	password := os.Getenv("RINGOVER_PASSWORD")
	dbName := os.Getenv("RINGOVER_DB")

	db := createDBAndTables(user, password, dbName)

	app := initApp(db)

	r := app.getRouter()
	r.Run(":3000")
}
