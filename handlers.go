package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type Todo struct {
	ID   int    `db:"id"`
	Task string `db:"task" form:"task" json:"task" binding:"required"`
}

func (app *Application) getTodos(c *gin.Context) {
	todos := []Todo{}

	limit := c.DefaultQuery("limit", "100")
	offset := c.DefaultQuery("offset", "0")
	query := fmt.Sprintf("SELECT * FROM todos ORDER BY id LIMIT %s OFFSET %s", limit, offset)

	err := app.DB.Select(&todos, query)
	if err != nil {
		app.logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, "Cannot retrieve todos list")
		return
	}

	c.JSON(http.StatusOK, todos)
}

func (app *Application) createTodo(c *gin.Context) {
	todo := Todo{}
	err := c.Bind(&todo)

	if err != nil {
		app.logger.Error(err.Error())
		c.JSON(http.StatusBadRequest, "Cannot create todo, incorrect parameters")
		return
	}

	_, err = app.DB.Exec("INSERT INTO todos (task) VALUES (?)", todo.Task)
	if err != nil {
		app.logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, "Cannot create todo")
		return
	}

	c.JSON(http.StatusOK, "created")
}

func (app *Application) deleteTodo(c *gin.Context) {
	id := c.Param("id")

	result, err := app.DB.Exec("DELETE FROM todos WHERE id = ?", id)
	if err != nil {
		app.logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, "Cannot delete todo")
		return
	}

	rowsQtt, err := result.RowsAffected()
	if err != nil {
		app.logger.Error(err.Error())
		c.JSON(http.StatusInternalServerError, "Cannot delete todo")
		return
	}

	if rowsQtt != 1 {
		app.logger.Info(fmt.Sprintf("row with id %s does not exists", id))
		c.JSON(http.StatusNotFound, fmt.Sprintf("row with id %s does not exists", id))
		return
	}

	app.logger.Sugar().Infof("todo with id %s has been successfully deleted", id)

	c.JSON(http.StatusOK, "deleted")
}
