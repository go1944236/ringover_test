package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type Application struct {
	DB     *sqlx.DB
	logger *zap.Logger
}

func initApp(DB *sqlx.DB) Application {
	return Application{
		DB:     DB,
		logger: initLogger(),
	}
}

func initLogger() *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	return logger
}

func (app *Application) getRouter() *gin.Engine {
	r := gin.Default()
	r.GET("/todolists", app.getTodos)
	r.POST("/todolists", app.createTodo)
	r.DELETE("/todolists/:id", app.deleteTodo)
	return r
}
