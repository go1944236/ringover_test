Launch the application with:\
RINGOVER_USER=mysqlUser RINGOVER_PASSWORD=mysqlPassword RINGOVER_DB=dbname go run .\
The database will be created at the start of the application if it does not exists, mysql should run on port 3306 beforehand.\
\
Curl commands:\
curl -d "task=description" -X POST http://localhost:3000/todolists  
curl -X DELETE http://localhost:3000/todolists/:id  
curl http://localhost:3000/todolists?limit=x&offset=y  
  
Launch tests with:  
RINGOVER_USER=mysqlUser RINGOVER_PASSWORD=mysqlPassword go test
