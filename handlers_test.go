package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
)

var DBtest *sqlx.DB

func TestMain(m *testing.M) {
	user := os.Getenv("RINGOVER_USER")
	password := os.Getenv("RINGOVER_PASSWORD")
	dbName := "ringover_test_test"
	DBtest = createDBAndTables(user, password, dbName)

	seedTest(DBtest)

	exitVal := m.Run()

	DBtest.MustExec(fmt.Sprintf("DROP DATABASE %s", dbName))

	os.Exit(exitVal)
}

func seedTest(DB *sqlx.DB) {
	for i := 0; i < 5; i++ {
		DB.MustExec(fmt.Sprintf("INSERT INTO todos (task) VALUES (%d)", i))
	}
}

func TestGetTodos(t *testing.T) {
	app := initApp(DBtest)
	r := app.getRouter()

	t.Run("should retrieve all tasks", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/todolists", nil)
		if err != nil {
			t.Fatal(err)
		}

		recorder := httptest.NewRecorder()

		r.ServeHTTP(recorder, req)

		var expected int
		err = DBtest.Get(&expected, "SELECT COUNT(*) FROM todos")
		if err != nil {
			t.Error(err)
		}

		var todos []Todo
		err = json.Unmarshal(recorder.Body.Bytes(), &todos)
		if err != nil {
			t.Error(err)
		}

		got := len(todos)

		if got != expected {
			t.Errorf("want to retrieve %d todos, only got %d", expected, got)
		}
	})

	t.Run("limit is working", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/todolists?limit=1&offset=1", nil)
		if err != nil {
			t.Fatal(err)
		}

		recorder := httptest.NewRecorder()

		r.ServeHTTP(recorder, req)

		var todos []Todo
		err = json.Unmarshal(recorder.Body.Bytes(), &todos)
		if err != nil {
			t.Error(err)
		}

		expected := 1
		got := len(todos)

		if got != expected {
			t.Errorf("want to retrieve %d todos, got %d", expected, got)
		}
	})

	t.Run("offset is working", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/todolists?offset=1", nil)
		if err != nil {
			t.Fatal(err)
		}

		recorder := httptest.NewRecorder()

		r.ServeHTTP(recorder, req)

		var expected int
		err = DBtest.Get(&expected, "SELECT COUNT(*) FROM (SELECT * FROM todos LIMIT 1000 OFFSET 1) as a")
		if err != nil {
			t.Error(err)
		}

		var todos []Todo
		err = json.Unmarshal(recorder.Body.Bytes(), &todos)
		if err != nil {
			t.Error(err)
		}

		got := len(todos)

		if got != expected {
			t.Errorf("want to retrieve %d todos, got %d", expected, got)
		}
	})
}

func TestCreateTodo(t *testing.T) {
	app := initApp(DBtest)
	r := app.getRouter()

	taskName := "TestCreateTodo"
	data := []byte(fmt.Sprintf(`task=%s`, taskName))
	recorder := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "/todolists", bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	if err != nil {
		t.Error(err)
	}

	r.ServeHTTP(recorder, req)

	var created bool
	err = DBtest.Get(&created, fmt.Sprintf("SELECT EXISTS(SELECT id FROM todos WHERE task = '%s')", taskName))
	if err != nil {
		t.Error(err)
	}

	if !created {
		t.Errorf("todo not created")
	}
}

func TestDeleteTodo(t *testing.T) {
	app := initApp(DBtest)
	r := app.getRouter()

	res := DBtest.MustExec("INSERT INTO todos (task) VALUES ('TestDeleteTodo')")
	id, err := res.LastInsertId()
	if err != nil {
		t.Error(err)
	}

	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("/todolists/%d", id), nil)
	if err != nil {
		t.Error(err)
	}
	recorder := httptest.NewRecorder()
	r.ServeHTTP(recorder, req)

	var deleted bool
	err = DBtest.Get(&deleted, fmt.Sprintf("SELECT NOT EXISTS(SELECT id FROM todos WHERE id = '%d')", id))
	if err != nil {
		t.Error(err)
	}

	if !deleted {
		t.Errorf("todo not deleted")
	}
}
